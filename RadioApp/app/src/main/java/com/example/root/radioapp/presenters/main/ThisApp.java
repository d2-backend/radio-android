package com.example.root.radioapp.presenters.main;

import android.app.Application;

/**
 * Created by root on 31.10.17.
 */

public class ThisApp extends Application {
    public static final String EXTRA_TOKEN_KEY = "authToken";
    public static final String SERVER_URL = "http://207.154.193.200:8080/api/v1/";
    public static final String EXTRA_PHONE_NUMBER_KEY = "phoneNumber";
    public static final String EXTRA_FACEBOOK_KEY = "facebook";
    public static final String EXTRA_PHONE_ID_KEY = "uniq_phone_id";

}
