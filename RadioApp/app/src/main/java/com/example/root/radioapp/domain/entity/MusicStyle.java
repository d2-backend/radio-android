package com.example.root.radioapp.domain.entity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eugene Garagulya
 * on 30.10.17.
 */

public class MusicStyle {

    //This entity can exist if it have name and id of user
    public MusicStyle(String name, int id) {
        this.name = name;
        this.id = id;
    }

    //When we create this entity after server API response, use only this constructor
    private MusicStyle(JSONObject jsonObject) throws JSONException {
        this.id = jsonObject.getInt("id");
        this.name = jsonObject.getString("name");
    }

    public final String name;
    public final int id;

    //Method which create list of Music styles
    public static List<MusicStyle> getMusicStyles() throws IOException, JSONException {
        //Initialized list which contains list of this entity
        List<MusicStyle> stylesList = new ArrayList<MusicStyle>();

        //Create connection to server API url
        URL url = new URL("http://207.154.193.200:8080/radiostations");
        //Open connection
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept-Charset", "UTF-8");
        try {
            InputStream inStream = new BufferedInputStream(connection.getInputStream());
            InputStreamReader reader = new InputStreamReader(inStream);

            int data = reader.read();
            StringBuilder response = new StringBuilder();

            while (data != -1) {
                char currentSymbol = (char) data;
                data = reader.read();
                response.append(currentSymbol);
            }

            JSONArray jsonArray = new JSONArray(response.toString());
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                stylesList.add(new MusicStyle(jsonObject));
            }
            return stylesList;
        } finally {
            connection.disconnect();
        }
    }
}
