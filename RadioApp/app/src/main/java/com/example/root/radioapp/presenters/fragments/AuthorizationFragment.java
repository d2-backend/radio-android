package com.example.root.radioapp.presenters.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.root.radioapp.R;
import com.example.root.radioapp.domain.util.Streams;
import com.example.root.radioapp.presenters.main.MainActivity;
import com.example.root.radioapp.presenters.main.ThisApp;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Eugene Garagulya
 * on 30.10.17.
 */

public class AuthorizationFragment extends Fragment {

    private final String TAG = getClass().getSimpleName();


    //FB auth
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private Button facebookLogin;

    //Phone auth
    private EditText phoneNumber;
    private Button btnNext;

    //Server API URL


    //SharedPreferences
    SharedPreferences token;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.authorization_fragment, parent, false);

        //FacebookSdk.sdkInitialize(getContext());
        callbackManager = CallbackManager.Factory.create();


        //FB auth process
        loginButton = rootView.findViewById(R.id.login_facebook_simple_btn);
        facebookLogin = rootView.findViewById(R.id.btn_facebook);
        loginButton.setFragment(this);
        loginButton.setReadPermissions(Arrays.asList("public_profile"));

        //Phone auth process
        phoneNumber = rootView.findViewById(R.id.et_phone_number);
        btnNext = rootView.findViewById(R.id.btn_next);


        facebookLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginButton.performClick();
            }
        });

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                //Log.d("FACEBOOK" + TAG, "onSuccess: Enter Method");
                AccessToken fbToken = loginResult.getAccessToken();
                GraphRequest request = GraphRequest.newMeRequest(fbToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            JSONObject json = new JSONObject();
                            json.put("id", object.getString("id"));
                            json.put("first_name", object.getString("first_name"));
                            json.put("last_name", object.getString("last_name"));
                            json.put("email", object.getString("email"));
                            json.put("photo", object.getJSONObject("picture").getJSONObject("data").getString("url"));
                            sendUserId(json);
                        } catch (JSONException e) {
                            Log.e(TAG, "Facebook(GraphReq) onCompleted: " + e.getMessage(), e);
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,email,picture");
                request.setParameters(parameters);
                request.executeAsync();

                goToMainScreen();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getContext(), R.string.cancel_login_fb, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getContext(), R.string.error_login_fb, Toast.LENGTH_SHORT).show();
            }
        });


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO -> discard comments, when server will work
                JSONObject json = new JSONObject();
                if (!phoneNumber.getText().toString().isEmpty()) {
                    if (Patterns.PHONE.matcher(phoneNumber.getText().toString()).matches()) {
                        try {
                            json.put("phone", phoneNumber.getText().toString());
                            sendUserId(json);
                        } catch (JSONException e) {
                            Log.e(TAG, "Phone auth, onClick: JSON_ERROR_PHONE" + e.getMessage(), e);
                        }
                        //Open new fragment
                        Bundle args = new Bundle();
                        args.putString(ThisApp.EXTRA_PHONE_NUMBER_KEY, phoneNumber.getText().toString());
                        PhoneMessageAuthorization nextAuthFragment = new PhoneMessageAuthorization();
                        nextAuthFragment.setArguments(args);
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.fragment_container, nextAuthFragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                    }

                } else {
                    Toast.makeText(getContext(), "Fill the phone number", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return rootView;
    }

//  Send request to server
    private void sendUserId(final JSONObject userJson) {
        new Thread(){
            @Override
            public void run() {
                try {
                    JSONObject responseJSONObj = Streams.loadJSONObjectFromURL(ThisApp.SERVER_URL + "auth", userJson);
                    if (responseJSONObj.getString("result").equals("success")) {
                        try {
                            token = getActivity().getPreferences(MODE_PRIVATE);
                            SharedPreferences.Editor editor = token.edit();
                            editor.putString(ThisApp.EXTRA_TOKEN_KEY, responseJSONObj.getString("token"));
                            editor.apply();
                        } catch (NullPointerException e) {
                            Log.e(TAG, "SendUserInfo method run: " + e.getMessage(), e);
                        }
                    }
                } catch (IOException e) {
                    Log.d(TAG, "run: IOException during send request");
                } catch (JSONException e) {
                    Log.e(TAG, "run: " + e.getMessage(), e);
                }
            }
        };
    }


    private void goToMainScreen() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    //for facebook auth, when webBrowser open, it return result of facebook login/cancel/error
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
