package com.example.root.radioapp.presenters.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.root.radioapp.R;
import com.example.root.radioapp.presenters.main.MainActivity;
import com.example.root.radioapp.presenters.main.ThisApp;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by root on 30.10.17.
 */

public class PhoneMessageAuthorization extends Fragment {

    private final String TAG = getClass().getSimpleName();

    SharedPreferences tokenSharedPref;

    TextView phoneNumberTextView;
    TextView recentNumberTextView;

    EditText messageCodeEditText;
    Button nextBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.phone_message_authorization_fragment, parent, false);


        phoneNumberTextView = rootView.findViewById(R.id.textView_phone_number);


        recentNumberTextView = rootView.findViewById(R.id.recent_code_btn);

        messageCodeEditText = rootView.findViewById(R.id.et_message_code);
        nextBtn = rootView.findViewById(R.id.btn_next_send_code);

        try {
            phoneNumberTextView.setText(getArguments().getString(ThisApp.EXTRA_PHONE_NUMBER_KEY));
        } catch (NullPointerException e) {
            Log.e(TAG, "SMS Code onCreateView: " + e.getMessage(), e);
        }

        recentNumberTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO -> resent message
                Toast.makeText(getContext(), "Message was send you again", Toast.LENGTH_SHORT).show();
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!messageCodeEditText.getText().toString().isEmpty()) {
                    final JSONObject json = new JSONObject();
                    try {
                        json.put("smsCode", messageCodeEditText.getText().toString());
                    } catch (JSONException e) {
                        Log.d("JSON_ERROR", "onClick: JSON_ERROR_PHONE");
                    }
                    new Thread(){
                        @Override
                        public void run() {
                            try {
                                URL url = new URL(ThisApp.SERVER_URL + "auth");
                                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                                connection.setDoOutput(true);
                                connection.setDoInput(true);
                                connection.setRequestMethod("POST");
                                try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
                                    wr.write(json.toString().getBytes());
                                }
                                InputStream in = new BufferedInputStream(connection.getInputStream());
                                InputStreamReader reader = new InputStreamReader(in);

                                int data = reader.read();
                                StringBuilder response = new StringBuilder();
                                while (data != -1) {
                                    char current = (char) data;
                                    data = reader.read();
                                    response.append(current);
                                }
                                try {
                                    //TODO -> response is JSON ?
                                    if (response.toString().equals("success")) {
                                        SharedPreferences token = getActivity().getPreferences(Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = token.edit();
                                        editor.putString(ThisApp.EXTRA_TOKEN_KEY, response.toString());
                                        editor.apply();
                                        goToMainScreen();
                                    }
                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                                connection.disconnect();
                            } catch (MalformedURLException e) {
                                Log.d("PhoneAuthFragment", "run: Malformed during send request");
                            } catch (IOException e) {
                                Log.d("PhoneAuthFragment", "run: IOException durind send request");
                            } finally {

                            }
                        }
                    };
                } else {
                    Toast.makeText(getContext(), "Fill message code", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return rootView;
    }
    private void goToMainScreen() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
