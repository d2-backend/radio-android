package com.example.root.radioapp.presenters.authorizationActivity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.example.root.radioapp.R;
import com.example.root.radioapp.presenters.fragments.AuthorizationFragment;

/**
 * Created by Eugene Garagulya
 * on 30.10.17.
 */

public class AuthorizationActivity extends FragmentActivity {

    FragmentManager fragmentManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.authorization_activity);

        fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        AuthorizationFragment fragment = new AuthorizationFragment();
        fragmentTransaction.add(R.id.fragment_container, fragment);
        fragmentTransaction.commit();
    }

}
