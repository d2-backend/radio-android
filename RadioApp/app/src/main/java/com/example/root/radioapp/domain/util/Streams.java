package com.example.root.radioapp.domain.util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Eugene Garagulya
 * on 20.11.17.
 */

public class Streams {

    public static JSONObject loadJSONObjectFromURL(String urlString, JSONObject data) throws IOException, JSONException {
        HttpURLConnection urlConnection = null;
        URL url = new URL(urlString);
        urlConnection = (HttpURLConnection) url.openConnection();
        if (data == null) {
            urlConnection.setRequestMethod("GET");
        } else {
            urlConnection.setRequestMethod("POST");
            try (DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream())) {
                wr.write(data.toString().getBytes());
            }
        }
        urlConnection.setReadTimeout(10000 /*miliseconds*/);
        urlConnection.setConnectTimeout(15000/*miliseconds*/);
        urlConnection.setDoOutput(true);
        urlConnection.connect();

        BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
        StringBuilder stringBuilder = new StringBuilder();

        String line;
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(data);
            stringBuilder.append("\n");
        }
        reader.close();

        String jsonString = stringBuilder.toString();
        urlConnection.disconnect();

        return new JSONObject(jsonString);
    }

}
