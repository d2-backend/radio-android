package com.example.root.radioapp.presenters.main;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.root.radioapp.R;
import com.example.root.radioapp.presenters.authorizationActivity.AuthorizationActivity;
import com.facebook.AccessToken;

public class MainActivity extends AppCompatActivity {

    SharedPreferences token;
    String tokenValue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_nav_view);

        tokenValue = getSharedPreferences(ThisApp.EXTRA_TOKEN_KEY, MODE_PRIVATE).getString(ThisApp.EXTRA_TOKEN_KEY, null);
        if (AccessToken.getCurrentAccessToken() == null || tokenValue == null) {
            goLoginScreen();
        } else {
            //TODO -> station list
        }


        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_stations:
                        Toast.makeText(MainActivity.this, "Stations", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.menu_question:
                        Toast.makeText(MainActivity.this, "Questions", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.menu_profile:
                        Toast.makeText(MainActivity.this, "Profile", Toast.LENGTH_SHORT).show();
                        break;
                }
                return true;
            }
        });
    }
    private void goLoginScreen() {
        Intent intent = new Intent(this, AuthorizationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
